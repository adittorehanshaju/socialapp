  const correct = document.createElement('i');
  correct.setAttribute("class", "fa fa-check fa-lg");

  const incorrect = document.createElement('i');
  incorrect.setAttribute("class", "fa fa-times fa-lg");
  
  function validateEmail(email) 
  {
      const re = /\S+@\S+\.\S+/;
      return re.test(email);
  }
  
  const input = document.querySelector('#email');  
  const status = document.querySelector('.status');
  const childNode = document.querySelector('.status i')
  
  input.addEventListener('input', updateValue);
  
  function correctEmail(){
      if(status.childNodes[0]) status.removeChild(status.childNodes[0])
      status.appendChild(correct)
      status.classList.add("good");
      status.classList.remove("hide");
      status.classList.remove("bad");      
      console.log("True")      
  }

  function inCorrectEmail(){
    if(status.childNodes[0]) status.removeChild(status.childNodes[0])
    status.appendChild(incorrect)
    status.classList.remove("good");
    status.classList.remove("hide");
    status.classList.add("bad");   
    console.log("False")
}

  function updateValue(e) {
    if(validateEmail(e.target.value)) {
        correctEmail()
    }
    else {
        inCorrectEmail()
    }
  }